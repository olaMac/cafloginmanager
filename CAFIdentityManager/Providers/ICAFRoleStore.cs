﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CAFIdentityManager.Providers
{
    public interface ICAFRoleStore<TRole> : IRoleStore<TRole, Int32>, IDisposable where TRole : Microsoft.AspNet.Identity.IRole<Int32>
    {
    }
}