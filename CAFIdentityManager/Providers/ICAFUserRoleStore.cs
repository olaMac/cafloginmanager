﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CAFIdentityManager.Providers
{
    public interface ICAFUserRoleStore<TUser> : IUserRoleStore<TUser, int>, IDisposable 
        where TUser : class, Microsoft.AspNet.Identity.IUser<int>           
    {
          
    }
}