﻿using CAFIdentityManager.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace CAFIdentityManager.Providers
{
    public class CAFUserStore : ICAFUserStore<CAFUser>, ICAFUserPasswordStore<CAFUser>, ICAFUserSecurityStampStore<CAFUser>,
        ICAFUserRoleStore<CAFUser>
    {
        MCIADBContext _context;
        MCIADBContext context
        {
            get
            {
                if (_context == null)
                    _context = new MCIADBContext();
                return _context;
            }
            set { _context = value; }
        }

        public CAFUserStore()
        {
            context = new MCIADBContext();
        }

        public CAFUserStore(DbContext context)
        {
            context = context as MCIADBContext;
        }

        public Task CreateAsync(CAFUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user cannot be null.");
            }

            if (string.IsNullOrEmpty(user.UserName))
            {
                throw new ArgumentNullException("username cannot be null or empty.");
            }

            if (string.IsNullOrEmpty(user.PasswordHash))
            {
                throw new ArgumentNullException("password cannot be null or empty.");
            }

            context.CAFUsers.Add(user);
            context.Configuration.ValidateOnSaveEnabled = false;
            return context.SaveChangesAsync();
        }

        public Task DeleteAsync(CAFUser user)
        {
            context.CAFUsers.Remove(user);
            context.Configuration.ValidateOnSaveEnabled = false;
            return context.SaveChangesAsync();
        }

        public Task<CAFUser> FindByIdAsync(int userId)
        {
            return context.CAFUsers.Where(u => u.Id == userId).FirstOrDefaultAsync();
        }

        public Task<CAFUser> FindByNameAsync(string userName)
        {
            return context.CAFUsers.Where(u => u.UserName.ToLower() == userName.ToLower()).FirstOrDefaultAsync();
        }

        public Task UpdateAsync(CAFUser user)
        {
            context.CAFUsers.Attach(user);
            context.Entry(user).State = EntityState.Modified;
            context.Configuration.ValidateOnSaveEnabled = false;
            return context.SaveChangesAsync();
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public Task<string> GetPasswordHashAsync(CAFUser user)
        {
            return context.CAFUsers.Where(a => a.Id == user.Id).Select(a => a.PasswordHash).FirstOrDefaultAsync();
        }

        public Task<bool> HasPasswordAsync(CAFUser user)
        {
            return context.CAFUsers.Where(a => a.Id == user.Id).Select(a => a.PasswordHash != null && a.PasswordHash != string.Empty).FirstOrDefaultAsync();
        }

        public Task SetPasswordHashAsync(CAFUser user, string passwordHash)
        {
            var userFromDB = context.CAFUsers.Where(a => a.Id == user.Id).Select(a => a).FirstOrDefault();
            if (userFromDB != null)
            {
                userFromDB.PasswordHash = passwordHash;
                return context.SaveChangesAsync();
            }
            userFromDB = context.CAFUsers.Where(a => a.UserName == user.UserName).Select(a => a).FirstOrDefault();
            if (userFromDB != null)
            {
                userFromDB.PasswordHash = passwordHash;
                return context.SaveChangesAsync();
            }

            user.PasswordHash = passwordHash;
            // user.Password = passwordHash;
            return Task.Factory.StartNew(() => { });
        }

        public Task<string> GetSecurityStampAsync(CAFUser user)
        {
            return context.CAFUsers.Where(a => a.Id == user.Id).Select(a => a.SecurityStamp).FirstOrDefaultAsync();
        }

        public Task SetSecurityStampAsync(CAFUser user, string stamp)
        {
            var userFromDB = context.CAFUsers.Where(a => a.Id == user.Id).Select(a => a).FirstOrDefault();
            if (userFromDB != null)
            {
                userFromDB.SecurityStamp = stamp;
                return context.SaveChangesAsync();
            }
            user.SecurityStamp = stamp;
            return Task.Factory.StartNew(() => { });
        }

        #region ICAFUserRoleStore

        public Task AddToRoleAsync(CAFUser user, int roleID)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            if (roleID <=0)
            {
                throw new ArgumentNullException("roleID");
            }

            var userRole = new CAFUserRole();
            userRole.RoleId = roleID;
            userRole.UserId = user.Id;
             
            // this should work, but it doesn't so instead it is done "manually"
          //  context.CAFUserRoles.Add(userRole);
             
            using(var conn = context.Database.Connection)
            {
                if (conn.State != System.Data.ConnectionState.Open)
                {
                    conn.Open();
                }
                System.Data.Common.DbCommand cmd = conn.CreateCommand();
                cmd.CommandText = string.Format("INSERT [dbo].[CAFUserRole]([CAFUser_ID], [CAFRole_ID]) VALUES({0}, {1})", user.Id, roleID);
                cmd.ExecuteNonQuery();

            }
 
          //  return context.SaveChangesAsync();
              return Task.FromResult<int>(0);  
        }

        public Task<IList<string>> GetRolesAsync(CAFUser user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            var result = (from userRoles in context.CAFUserRoles
                          join role in context.CAFRoles on userRoles.RoleId equals role.Id
                          where userRoles.UserId == user.Id
                          select role.Name).ToList();
            return Task.FromResult<IList<string>>(result);
        }       

        public Task<bool> IsInRoleAsync(CAFUser user, string roleName)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            if (String.IsNullOrEmpty(roleName))
            {
                throw new ArgumentNullException("roleName");
            }
            throw new NotImplementedException();
            //var role = context.CAFUserRoles.Where(a => a.Name == roleName).FirstOrDefault();
            //if (role == null)
            //{
            //    throw new InvalidOperationException("Role not found");
            //}
        }

        public Task<bool> IsInRoleAsync(CAFUser user, CAFRole role)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            if (role == null)
            {
                throw new ArgumentNullException("role");
            }

            return context.CAFUserRoles.Where(a => a.UserId == user.Id && a.RoleId == role.Id).Select(a => a.ID > 0).FirstOrDefaultAsync();
        }

        public Task RemoveFromRoleAsync(CAFUser user, int roleID)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }

            if (roleID <=0)
            {
                throw new ArgumentNullException("roleID");
            }
             
            var userRole = context.CAFUserRoles.Where(a => a.UserId == user.Id && a.RoleId == roleID).FirstOrDefault();
            if (userRole == null)
            {
                throw new InvalidOperationException("UserRole not found");
            }
            context.CAFUserRoles.Remove(userRole);
            return context.SaveChangesAsync();
        }

        public Task AddToRoleAsync(CAFUser user, string roleName)
        {
            var role = context.CAFRoles.Where(a => a.Name == roleName).FirstOrDefault();
            return AddToRoleAsync(user, role.Id);
        }

        public Task RemoveFromRoleAsync(CAFUser user, string roleName)
        {
            var role = context.CAFRoles.Where(a => a.Name == roleName).FirstOrDefault();
            return RemoveFromRoleAsync(user, role.Id);
        }

        #endregion ICAFUserRoleStore

    }
}