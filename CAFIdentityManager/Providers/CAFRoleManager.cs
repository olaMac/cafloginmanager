﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CAFIdentityManager.Providers
{
    public class CAFRoleManager<TRole> : RoleManager<TRole, Int32> 
        where TRole : class, Microsoft.AspNet.Identity.IRole<Int32>
    {

        public CAFRoleManager(ICAFRoleStore<TRole> store)
            : base(store)
        {
        }
    }
}