﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CAFIdentityManager.Providers
{

    public class CAFUserManager<TUser> : UserManager<TUser, Int32> 
        where TUser : class, Microsoft.AspNet.Identity.IUser<Int32>
    {

        public CAFUserManager(ICAFUserStore<TUser> store) : base(store)
        {
        }
   
    }
}