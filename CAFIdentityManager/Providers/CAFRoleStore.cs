﻿using CAFIdentityManager.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CAFIdentityManager.Providers
{
    public class CAFRoleStore : ICAFRoleStore<CAFRole>
    {
        MCIADBContext _context;
        MCIADBContext context
        {
            get
            {
                if (_context == null)
                    _context = new MCIADBContext();
                return _context;
            }
            set { _context = value; }
        }

        public CAFRoleStore()
        {
            context = new MCIADBContext();
        }

        public CAFRoleStore(DbContext context)
        {
            context = context as MCIADBContext;
        }

        public System.Threading.Tasks.Task CreateAsync(CAFRole role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("role cannot be null.");
            }

            if (string.IsNullOrEmpty(role.Name))
            {
                throw new ArgumentNullException("role name cannot be null or empty.");
            }

            context.CAFRoles.Add(role);
            context.Configuration.ValidateOnSaveEnabled = false;
            return context.SaveChangesAsync();
        }

        public System.Threading.Tasks.Task DeleteAsync(CAFRole role)
        {
            context.CAFRoles.Remove(role);
            context.Configuration.ValidateOnSaveEnabled = false;
            return context.SaveChangesAsync();
        }

        public System.Threading.Tasks.Task<CAFRole> FindByIdAsync(int roleId)
        {
            return context.CAFRoles.Where(u => u.Id == roleId).FirstOrDefaultAsync();
        }

        public System.Threading.Tasks.Task<CAFRole> FindByNameAsync(string roleName)
        {
            return context.CAFRoles.Where(u => u.Name.ToLower() == roleName.ToLower()).FirstOrDefaultAsync();
        }

        public System.Threading.Tasks.Task UpdateAsync(CAFRole role)
        {
            context.CAFRoles.Attach(role);
            context.Entry(role).State = EntityState.Modified;
            context.Configuration.ValidateOnSaveEnabled = false;
            return context.SaveChangesAsync();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}