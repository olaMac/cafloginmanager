﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CAFIdentityManager.Providers
{
    public interface ICAFUserStore<TUser> : IUserStore<TUser, Int32>, IDisposable 
        where TUser : class, Microsoft.AspNet.Identity.IUser<Int32>
    {
    }
}