﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CAFIdentityManager.Models
{
    public class CAFUserClaim: IdentityUserClaim<Int32>
    {
        [Key]
        public int ID { get; set; }
    }    
}