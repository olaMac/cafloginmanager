﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CAFIdentityManager.Models
{
 
    public class CAFUserRole : IdentityUserRole<Int32>
    {
        [Key]
        public int ID { get; set; }

        //public int CAFUser_ID { get; set; }

        //public int CAFRole_ID { get; set; }
    }
}