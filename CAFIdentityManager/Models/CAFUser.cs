﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CAFIdentityManager.Models
{

    public class CAFUser : IdentityUser<Int32, CAFUserLogin, CAFUserRole, CAFUserClaim>        
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int CAFAccount_ID { get; set; }
      
        public CAFUser()
        {
           
        }
    }

}