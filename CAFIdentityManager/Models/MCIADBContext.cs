﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CAFIdentityManager.Models
{
    public class MCIADBContext : DbContext
    {

        public MCIADBContext(): base("name=MCIADBContext")
        {            
        }

        public virtual IDbSet<CAFIdentityManager.Models.CAFUser> CAFUsers { get; set; }
        public virtual IDbSet<CAFIdentityManager.Models.CAFRole> CAFRoles { get; set; }
        public virtual IDbSet<CAFIdentityManager.Models.CAFUserRole> CAFUserRoles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder); 

            modelBuilder.Entity<CAFUser>().ToTable("CAFUser");

            modelBuilder.Entity<CAFUser>().Property(t => t.PasswordHash).HasColumnName("Password");

            // ROLES
            modelBuilder.Entity<CAFRole>().ToTable("CAFRole");
            //modelBuilder.Entity<IdentityUserRole>().ToTable("CAFUserRole");
            //modelBuilder.Entity<IdentityUserRole>().Property(t => t.UserId).HasColumnName("CAFUser_ID");
            //modelBuilder.Entity<IdentityUserRole>().Property(t => t.RoleId).HasColumnName("CAFRole_ID");
 

            modelBuilder.Entity<CAFUserRole>().ToTable("CAFUserRole");
            modelBuilder.Entity<CAFUserRole>().Property(t => t.UserId).HasColumnName("CAFUser_ID");
            modelBuilder.Entity<CAFUserRole>().Property(t => t.RoleId).HasColumnName("CAFRole_ID");
            modelBuilder.Entity<CAFUserRole>().HasKey<int>(r => r.ID);
             //modelBuilder.Entity<IntUserRole>().ToTable("CAFUserRole");
            //modelBuilder.Entity<IntUserRole>().Property(t => t.UserId).HasColumnName("CAFUser_ID");
            //modelBuilder.Entity<IntUserRole>().Property(t => t.RoleId).HasColumnName("CAFRole_ID");
            //modelBuilder.Entity<IdentityUserRole>().HasKey(r => new { r.RoleId, r.UserId });
            //modelBuilder.Entity<IdentityUserRole>().Property(t => t.UserId).HasColumnName("CAFUser_ID");
            //modelBuilder.Entity<IdentityUserRole>().Property(t => t.RoleId).HasColumnName("CAFRole_ID");

        }
    
    }
}
