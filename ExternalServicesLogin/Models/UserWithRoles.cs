﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExternalServicesLogin.Models
{
    public class UserWithRoles
    {
        public string Username { get; set; }
        public int UserID { get; set; }

        public List<UserRoleModel> UserInRole { get; set; }
    }

    public class UserRoleModel
    {
        public string RoleName { get; set; }
        public int RoleID { get; set; }
        public string IsInRole { get; set; }
        public string ActionName { get; set; }
        public string ActionMethodName { get; set; }
    }
}