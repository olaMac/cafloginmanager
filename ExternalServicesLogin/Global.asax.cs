﻿using CAFIdentityManager.Models;
using ExternalServicesLogin.Models;
using System.Data.Entity;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ExternalServicesLogin
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // can be nothing or
            Database.SetInitializer<MCIADBContext>(null);
            //Database.SetInitializer<YourDatabaseContext>(new DropCreateDatabaseIfModelChanges<YourDatabaseContext>());
        }
    }
}
