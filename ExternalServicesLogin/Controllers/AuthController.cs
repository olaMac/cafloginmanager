﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using ExternalServicesLogin.Models;
using CAFIdentityManager.Models;

namespace ExternalServicesLogin.Controllers
{
    /*
    To add a route for this controller, merge these statements into the Register method of the WebApiConfig class. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using ExternalServicesLogin.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<UserModel>("Auth");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    public class AuthController : ODataController
    {
        private MCIADBContext db = new MCIADBContext();

        // GET odata/Auth
        [Queryable]
        public IQueryable<CAFUser> GetAuth()
        {
            return db.CAFUsers;
        }

        // GET odata/Auth(5)
        [Queryable]
        public SingleResult<CAFUser> GetUserModel([FromODataUri] int key)
        {
            return SingleResult.Create(db.CAFUsers.Where(usermodel => usermodel.Id == key));
        }

        // PUT odata/Auth(5)
        public IHttpActionResult Put([FromODataUri] int key, CAFUser usermodel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (key != usermodel.Id)
            {
                return BadRequest();
            }

            db.Entry(usermodel).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserModelExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(usermodel);
        }

        // POST odata/Auth
        public IHttpActionResult Post(CAFUser usermodel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.CAFUsers.Add(usermodel);
            db.SaveChanges();

            return Created(usermodel);
        }

        // PATCH odata/Auth(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<CAFUser> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            CAFUser usermodel = db.CAFUsers.Find(key);
            if (usermodel == null)
            {
                return NotFound();
            }

            patch.Patch(usermodel);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserModelExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(usermodel);
        }

        // DELETE odata/Auth(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            CAFUser usermodel = db.CAFUsers.Find(key);
            if (usermodel == null)
            {
                return NotFound();
            }

            db.CAFUsers.Remove(usermodel);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserModelExists(int key)
        {
            return db.CAFUsers.Count(e => e.Id == key) > 0;
        }
    }
}
