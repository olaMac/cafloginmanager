﻿using CAFIdentityManager.Models;
using CAFIdentityManager.Providers;
using ExternalServicesLogin.Models;
using ExternalServicesLogin.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace ExternalServicesLogin.Controllers
{


    [Authorize]
    public class DBTestController : Controller
    {

        [Authorize]
        public ActionResult Index()
        {
            StringListModel model = new StringListModel();
            if (Request.IsAuthenticated)
            {
                var dbContext = new MCIADBContext();
                var users = dbContext.CAFUsers.ToList();

                model.ListOfString = new List<string>();
                foreach (var user in users)
                {
                    if (user != null && user.UserName != null)
                        model.ListOfString.Add(user.UserName);
                }
            }
            return View(model);
        }

        [Authorize(Roles = "Administrator")]
        public ContentResult AdminOnly()
        {
            return Content("For Admin Only");
        }

         [Authorize]
        public ActionResult EditUser(string username)
        {
            if (string.IsNullOrWhiteSpace(username))
            {
                return RedirectToAction("Index");
            }
            //var userManager = new CAFUserManager<CAFUser>(new CAFUserStore());
            var userStore = new CAFUserStore();
            var user = userStore.FindByNameAsync(username).Result;

            var dbContext = new MCIADBContext();
            var roles = dbContext.CAFRoles.ToList();

            UserWithRoles model = new UserWithRoles();
            model.UserInRole = new List<UserRoleModel>();
            model.Username = username;
            model.UserID = user.Id;

            if (user != null && roles != null && roles.Count() > 0)
            {
                foreach (var role in roles)
                {
                    bool result =  userStore.IsInRoleAsync(user, role).Result;
                    model.UserInRole.Add(new UserRoleModel()
                    {
                        RoleID = role.Id,
                        RoleName = role.Name,
                        ActionMethodName = result ? "RemoveUserRole" : "AddUserRole",
                        ActionName = result ? "Remove" : "Add",
                        IsInRole = result ? "YES" : "NO"
                    });
                }
            }
            return View(model);
        }

         [Authorize]
         public ActionResult RemoveUserRole(int userID, int roleID)
         {
             var cafUserStore = new CAFUserStore();

             var user = cafUserStore.FindByIdAsync(userID).Result;

             cafUserStore.RemoveFromRoleAsync(user, roleID);

             return RedirectToAction("Index");
         }


         [Authorize]
         public ActionResult AddUserRole(int userID, int roleID)
         {
             var cafUserStore = new CAFUserStore();

             var user = cafUserStore.FindByIdAsync(userID).Result;

             cafUserStore.AddToRoleAsync(user, roleID);

             return RedirectToAction("Index");
         }
    }
}