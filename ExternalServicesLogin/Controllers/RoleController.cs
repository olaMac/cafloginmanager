﻿using CAFIdentityManager.Models;
using CAFIdentityManager.Providers;
using ExternalServicesLogin.Models;
using ExternalServicesLogin.Providers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace ExternalServicesLogin.Controllers
{
    [Authorize]
    public class RoleController : Controller
    {
        public CAFRoleManager<CAFRole> roleManager { get; private set; }

        public RoleController()
        {
            roleManager = new CAFRoleManager<CAFRole>(new CAFRoleStore(new MCIADBContext()));
        }

        //
        // GET: /Role/
        public ActionResult Index()
        {
            StringListModel model = new StringListModel();
             
            var dbContext = new MCIADBContext();
            var items = dbContext.CAFRoles.ToList();

            model.ListOfString = new List<string>();
            foreach (var entry in items)
            {
                if (entry != null && entry.Name != null)
                    model.ListOfString.Add(entry.Name);
            }

            return View(model);
        }

        public ActionResult NewRole()
        {
            CAFRole role = new CAFRole();
            return View(role);
        }


        [HttpPost]
        public ActionResult NewRole(CAFRole model)
        {
           if(ModelState.IsValid)
           {
               roleManager.CreateAsync(model);       
           }
           return RedirectToAction("Index");
        }

        public ActionResult DeleteRole(string roleName)
        {
            var role = roleManager.FindByName(roleName);
            roleManager.DeleteAsync(role);
            return RedirectToAction("Index");
        }
    }
}